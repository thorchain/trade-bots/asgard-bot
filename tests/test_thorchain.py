import unittest

from clients.thorchain import Pool


class TestBot(unittest.TestCase):
    def test_emit_amt(self):
        pool = Pool("BNB.BNB", 100 * 1e8, 100 * 1e8)

        amt = pool._calc_asset_emission(10 * 1e8, "BNB.BNB")
        self.assertEqual(amt, 826446280)
