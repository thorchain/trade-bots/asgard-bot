import logging
import time
import os
from util.common import HttpClient, Asset, OrderBook

from binance_chain.http import HttpApiClient
from binance_chain.wallet import Wallet
from binance_chain.messages import LimitOrderBuyMsg, LimitOrderSellMsg, CancelOrderMsg
from binance_chain.environment import BinanceEnvironment


class BinanceDex(HttpClient):
    """
    A local simple implementation of binance dex
    """

    chain = "BNB"

    def __init__(self, rune="BNB.RUNE-B1A"):
        self.rune = Asset(rune)
        env = BinanceEnvironment.get_production_env()
        self.client = HttpApiClient(env=env)
        self.markets = []
        self.wallet = Wallet.create_wallet_from_mnemonic(
            os.environ.get("MNEMONIC"), env=env
        )
        logging.info(f"Loaded Binance wallet: {self.wallet.address}")

    def get_market(self, asset):
        if isinstance(asset, str):
            asset = Asset(asset)
        sym = asset.get_symbol()
        if sym == "BNB":
            sym = "RUNE-B1A"
        for market in self.markets:
            base = market["base_asset_symbol"]
            quote = market["quote_asset_symbol"]
            if (sym == base and quote == "BNB") or (sym == quote and base == "BNB"):
                return market
        logging.error(f"No market found: {asset}")
        return None

    def get_symbol(self, asset):
        market = self.get_market(asset)
        if market is None:
            return ""
        base = market["base_asset_symbol"]
        quote = market["quote_asset_symbol"]
        return f"{base}_{quote}"

    def round_down_lot_size(self, asset, quantity):
        """
        Rounds down to the nears lot size. "Lot size" is the minimum trading
        size set by the binance dex. Each asset has a unique "lot size".
        """
        market = self.get_market(asset)
        if market is None:
            return 0
        lot_size = float(market["lot_size"])
        return int(quantity / (lot_size * 1e8)) * lot_size

    def round_down_tick_size(self, asset, price):
        """
        Rounds down to the nears tick size. "Tick size" is the minimum trading
        price set by the binance dex. Each asset has a unique "tick size".
        """
        market = self.get_market(asset)
        if market is None:
            return 0
        tick_size = float(market["tick_size"])
        price = int(price / tick_size) * tick_size
        return round(price, len(format(tick_size, 'f'))-1)

    def get_order_book(self, asset):
        data = self.client.get_order_book(self.get_symbol(asset))
        if len(data["asks"]) == 0 or len(data["bids"]) == 0:
            return OrderBook()
        return OrderBook.from_binance_dex_json(data)

    def update_markets(self):
        self.markets = self.client.get_markets()

    def get_order(self, order_id):
        return self.client.get_order(order_id)

    def order_complete(self, order_id):
        order = self.get_order(order_id)
        return order["status"] == "FullyFill"

    def wait_for_order(self, order_id, timeout=30):
        time.sleep(2)
        for x in range(0, timeout):
            if self.order_complete(order_id):
                return True
            time.sleep(1)
        return False

    def limit_order(self, asset, price, coin):
        market = self.get_market(asset)
        if market is None:
            return None

        if coin.asset.is_bnb():
            if market["base_asset_symbol"] == "BNB":
                return self.limit_sell_order(asset, price, coin.amount)
            elif market["quote_asset_symbol"] == "BNB":
                coin.amount = coin.amount * price
                return self.limit_buy_order(asset, price, coin.amount)
            else:
                return None
        else:
            if market["base_asset_symbol"] == "BNB":
                coin.amount = coin.amount * price
                return self.limit_buy_order(asset, price, coin.amount)
            elif market["quote_asset_symbol"] == "BNB":
                return self.limit_sell_order(asset, price, coin.amount)
            else:
                return None

    def limit_sell_order(self, asset, p, q):
        self.wallet.reload_account_sequence()
        market = self.get_market(asset)
        if market is None:
            return None

        quantity = self.round_down_lot_size(asset, q)
        if quantity <= 0:
            logging.error(f"Sell order quantity failure: {q} {asset} --> {quantity}")
            return None

        price = self.round_down_tick_size(asset, p)
        if price <= 0:
            logging.error(f"Sell order price failure: {p} {asset} --> {price}")
            return None

        logging.info(f"Binance Sell Order: {quantity} {self.get_symbol(asset)} @ {price}")
        order = LimitOrderSellMsg(
            wallet=self.wallet,
            symbol=self.get_symbol(asset),
            price=price,
            quantity=quantity,
        )
        resp = self.client.broadcast_msg(order, sync=True)
        time.sleep(
            0.3
        )  # sleep so we don't reuse a sequence number (binance-chain py mod bug)
        return resp

    def limit_buy_order(self, asset, p, q):
        self.wallet.reload_account_sequence()
        quantity = self.round_down_lot_size(asset, q)
        if quantity <= 0:
            logging.error(f"Buy order quantity failure: {q} {asset} --> {quantity}")
            return None

        price = self.round_down_tick_size(asset, p)
        if price <= 0:
            logging.error(f"Buy order price failure: {p} {asset} --> {price}")
            return None

        logging.info(f"Binance Buy Order: {quantity} {self.get_symbol(asset)} @ {price}")
        order = LimitOrderBuyMsg(
            wallet=self.wallet,
            symbol=self.get_symbol(asset),
            price=price,
            quantity=quantity,
        )
        resp = self.client.broadcast_msg(order, sync=True)
        time.sleep(
            0.3
        )  # sleep so we don't reuse a sequence number (binance-chain py mod bug)
        return resp

    def cancel_order(self, order_id, asset):
        self.wallet.reload_account_sequence()
        order = CancelOrderMsg(
            wallet=self.wallet, order_id=order_id, symbol=self.get_symbol(asset),
        )
        resp = self.client.broadcast_msg(order, sync=True)
        time.sleep(
            0.3
        )  # sleep so we don't reuse a sequence number (binance-chain py mod bug)
        return resp
